﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Basket.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Basket.DAL.Entities;

namespace Basket.Controllers
{
    public class HomeController : Controller
    {
        //private readonly EventBasketContext _context;


        EventBasketContext db;
        public HomeController(EventBasketContext context)
        {
            db = context;
        }

        public IActionResult Index(int? SelectedCities)
        {
            var cities = db.Cities.OrderBy(q => q.Name).ToList();
            ViewBag.SelectedCities = new SelectList(cities, "CityId", "Name", SelectedCities);
            int cityId = SelectedCities.GetValueOrDefault();

            IQueryable<Venue> venues = db.Venues
               .Where(c => !SelectedCities.HasValue || c.CityId == cityId)
               .OrderBy(d => d.VenueId)
               .Include(d => d.Cities);
            return View(venues.ToList());
        }

        public IActionResult Events(int? selectedvenues)
        {
            var venues = db.Venues.OrderBy(q => q.Name).ToList();
            ViewBag.selectedvenues = new SelectList(venues, "VenueId", "Name", selectedvenues);
            int venueId = selectedvenues.GetValueOrDefault();

            IQueryable<Event> events = db.Events
                .Where(c => !selectedvenues.HasValue || c.VenueId == venueId)
                .OrderBy(d => d.EventId)
                .Include(d => d.Venues);
            //return View(events.ToList());
            return View(db.Events.ToList());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
