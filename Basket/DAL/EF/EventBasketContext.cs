﻿using Basket.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basket.DAL.EF
{
    public class EventBasketContext : DbContext
    {
        public EventBasketContext()
        {
        }

        public EventBasketContext(DbContextOptions<EventBasketContext> options) : base(options) { }

        public DbSet<City> Cities { get; set; }
        public DbSet<Event> Events { get; set; }
        //public DbSet<Order> Orders { get; set; }
        //public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(EventBasketContext modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>()

            modelBuilder.Entity<User>()
                .HasMany(r => r.Roles)
                .WithOne(u => u.Users)
                Map()
        }
    }
}
