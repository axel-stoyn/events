﻿using Basket.DAL.EF;
using Basket.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Basket.Models
{
    public static class HardCodeDates
    {
        public static void Initialize(EventBasketContext context)
        {
            var city1 = new City
            {
                CityId = 1,
                Name = "Los Angeles"
            };
            var city2 = new City
            {
                CityId = 2,
                Name = "Detroid"
            };
            var city3 = new City
            {
                CityId = 3,
                Name = "Manhattan"
            };
            var city4 = new City
            {
                CityId = 4,
                Name = "New York"
            };
            var city5 = new City
            {
                CityId = 5,
                Name = "National City"
            };
            var city6 = new City
            {
                CityId = 6,
                Name = "Central City"
            };

            //context.Cities.AddRange(city1, city2, city3, city4, city5, city6);
            //context.Cities.Add(city1);
            //context.Cities.Add(city2);
            //context.Cities.Add(city3);
            //context.Cities.Add(city4);
            //context.Cities.Add(city5);
            //context.Cities.Add(city6);
            //context.SaveChanges();


            //Vanues for first city
            var venue1 = new Venue
            {
                VenueId = 1,
                Name = "National Hall",
                Address = "La Plaza",
                CityId = city1.CityId
            };
            var venue2 = new Venue
            {
                VenueId = 2,
                Name = "La Plaza",
                Address = "Sanset street",
                CityId = city1.CityId
            };

            //Venues for second city
            var venue3 = new Venue
            {
                VenueId = 3,
                Name = "Ford Field",
                Address = "Madison street",
                CityId = city2.CityId
            };
            var venue4 = new Venue
            {
                VenueId = 4,
                Name = "DTE Plaza",
                Address = "Bagly avenues",
                CityId = city2.CityId
            };

            //Venues for thirth city
            var venue5 = new Venue
            {
                VenueId = 5,
                Name = "Park Place",
                Address = "Barkly street",
                CityId = city3.CityId
            };
            var venue6 = new Venue
            {
                VenueId = 6,
                Name = "Broodway",
                Address = "Biver street",
                CityId = city3.CityId
            };

            //Venues for fourth city
            var venue7 = new Venue
            {
                VenueId = 7,
                Name = "Ambasador",
                Address = "7 avenues",
                CityId = city4.CityId
            };
            var venue8 = new Venue
            {
                VenueId = 8,
                Name = "Times Square",
                Address = "11 avenues",
                CityId = city4.CityId
            };

            //Venues for fifth city
            var venue9 = new Venue
            {
                VenueId = 9,
                Name = "Matthew's",
                Address = "12th street Lofts",
                CityId = city5.CityId
            };
            var venue10 = new Venue
            {
                VenueId = 10,
                Name = "West Hall",
                Address = "12th street Barber",
                CityId = city5.CityId
            };

            //Venues for sixth city
            var venue11 = new Venue
            {
                VenueId = 11,
                Name = "Hilton",
                Address = "Charles Avenue",
                CityId = city6.CityId
            };
            var venue12 = new Venue
            {
                VenueId = 12,
                Name = "St. Pierre",
                Address = "Royal Street",
                CityId = city6.CityId

            };

            var events1 = new Event
            {
                EventId = 1,
                Name = "Champion CUP",
                Description = "ahz",
                VenueId = venue1.VenueId,
                Date = Convert.ToDateTime("29/03/2018 23:00")
            };
            var events2 = new Event
            {
                EventId = 2,
                Name = "Champion WORLD",
                Description = "hz",
                VenueId = venue2.VenueId,
                Date = Convert.ToDateTime("31/03/2018 6:00")
            };

            var user1 = new User
            {
                Id = 1,
                FirstName = "Slava",
                LastName = "Vyacheslav",
                Email = "axel.stoyn@gmail.com",
                Password = "alskdjfhg",
                Username = "axel-stoyn",
                Address = "Budennogo street",
                Localization = "RU",
                PhoneNamber = "+375259778602"
            };

            context.Venues.AddRange(venue1, venue2, venue3, venue4, venue5, venue6, venue7, venue8, venue9, venue10, venue11, venue12);
            context.Events.AddRange(events1, events2);
            //context.Users.Add(user1);
            context.SaveChanges();
                
        }
    }
}
